#include "image.h"
#include "reader.h"
#include "rotate.h"
#include "transform.h"
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>

#if 0
int main( int argc, char** argv ) {
    (void) argc; (void) argv; // suppress 'unused parameters' warning
    struct image* im = image_create_filled(100, 200, pixel_create(0, 0, 0));
    struct pixel px = *pixel_create(0, 200, 255);
    image_set_pixel(im, 50, 40, px);
    uint8_t *pg1 = pixel_as_array(image_get_pixel(im, 50, 40));
    uint8_t *pg2 = pixel_as_array(image_get_pixel(im, 51, 41));
    printf("at 50, 40 c=%" PRIu8 ", %" PRIu8 ", %" PRIu8 "\n at 51,41 c=%" PRIu8 ", %" PRIu8 ", %" PRIu8 "\n",
           pg1[0], pg1[1], pg1[2], pg2[0], pg2[1], pg2[2]);
    image_free(im);

    return 0;
}
#else
int main(int argc, char **argv) {
    if (argc != 3) {
        printf("Wrong number of arguments. Expected: 2, got: %i\n", argc - 1);
        return 1;
    }
    struct image image;
    enum read_status read_result = from_bmp_path(argv[1], &image);
    if (read_result != READ_OK) {
        printf("Input file reading error %i\n", read_result);
        return 2;
    }

    struct image rotated;
    transform(&image, &rotated, &image_rotate);

    enum write_status write_result = to_bmp_path(argv[2], &rotated);
    if (write_result != WRITE_OK) {
        printf("Output file writing error %i\n", write_result);
    }
    image_free(&image);
    image_free(&rotated);
    printf("Done\n");
    return 0;
}
#endif
