//
// Created by volt200211 on 26.01.2022.
//

#include "transform.h"

void transform(struct image const *src, struct image* dst, void (*f)(struct image const* image, struct image* out_image)) {
    f(src, dst);
}
