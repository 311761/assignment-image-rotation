#include "rotate.h"
#include "image.h"
#include <stdlib.h>

void image_rotate(struct image const *image, struct image *out_image) {
    struct image* img_pointer = image_create(image->height, image->width);
    *out_image = *img_pointer;
    free(img_pointer);
    for (uint32_t i = 0; i < image->height; i++) {
        for (uint32_t j = 0; j < image->width; j++) {
            image_set_pixel(out_image, i, j, *image_get_pixel(image, j, image->height - 1 - i));
        }
    }
}
