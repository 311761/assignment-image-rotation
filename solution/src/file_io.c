#include "file_io.h"
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>

//enum open_status;
bool file_exists(const char *filename) {
    struct stat buffer;
    return (stat(filename, &buffer) == 0);
}

static enum open_status open_file(const char *fname, FILE **stream, const char *mode) {
    *stream = fopen(fname, mode);
    if(*stream == NULL)
        return OPEN_ERROR;
    return OPEN_OK;
}

enum open_status open_file_read(const char *fname, FILE **stream) {

    if (file_exists(fname))
        return open_file(fname, stream, "rb");
    return OPEN_INVALID_PATH;
}

enum open_status open_file_write(const char *fname, FILE **stream) {
    return open_file(fname, stream, "wb");
}
