#include "image.h"
#include <stdlib.h>

struct pixel *pixel_create(uint8_t r, uint8_t g, uint8_t b) {
    struct pixel *new_pixel = malloc(sizeof(struct pixel));
    *new_pixel = (struct pixel){r, g, b};
    return new_pixel;
}

struct image *image_create(uint64_t width, uint64_t height) {
    struct image *new_image = malloc(sizeof(struct image));
    new_image->data = malloc(sizeof(struct pixel) * width * height);
    new_image->width = width;
    new_image->height = height;
    return new_image;
}

struct image *image_create_filled(uint64_t width, uint64_t height, struct pixel *color) {
    struct image *new_image = image_create(width, height);
    for (struct pixel *current_pixel = new_image->data; current_pixel < new_image->data + width * height; current_pixel++)
        *current_pixel = *color;
    return new_image;
}

static bool image_valid_coords(struct image const *im, uint32_t x, uint32_t y) {
    return !(x >= im->width || y >= im->height);
}

struct pixel *image_get_pixel(struct image const *im, uint32_t x, uint32_t y) {
    if (image_valid_coords(im, x, y))
        return &(im->data[y * im->width + x]);
    return NULL;
}

bool image_set_pixel(struct image *im, uint32_t x, uint32_t y, struct pixel value) {
    if (!image_valid_coords(im, x, y))
        return false;
    im->data[y * im->width + x] = value;
    return true;
}

uint8_t *pixel_as_array(struct pixel *p) {
    return (uint8_t *) p;
}

void image_free(struct image *im) {
    free(im->data);
}
