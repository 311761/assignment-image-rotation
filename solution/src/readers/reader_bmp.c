#include "file_io.h"
#include "image.h"
#include "reader.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#define STANDART_BF_TYPE 19778
#define HEADER_SIZE 40
#define STANDART_PLANES 1
#define STANDART_PELS_PER_PIXEL 2834
#define BITS_IN_BYTE 8

#pragma pack(push, 1)
struct bmp_header// __attribute__((packed))
{
uint16_t bfType;
uint32_t bfileSize;
uint32_t bfReserved;
uint32_t bOffBits;
uint32_t biSize;
uint32_t biWidth;
uint32_t biHeight;
uint16_t biPlanes;
uint16_t biBitCount;
uint32_t biCompression;
uint32_t biSizeImage;
uint32_t biXPelsPerMeter;
uint32_t biYPelsPerMeter;
uint32_t biClrUsed;
uint32_t biClrImportant;
};
#pragma pack(pop)

uint8_t calculate_padding( uint32_t width) {
    uint64_t bite_row_size = sizeof(struct pixel)*width;
    return ((bite_row_size+3)/4)*4-bite_row_size;
}

/*  deserializer   */
//enum read_status;

enum read_status from_bmp( FILE* in, struct image* img ) {
    if(!in) {
        return READ_INVALID_FILE;
    }
    struct bmp_header* header = malloc(sizeof(struct bmp_header));
    fread(header, sizeof(struct bmp_header), 1, in);

    if ((*header).biBitCount != sizeof(struct pixel)*BITS_IN_BYTE)
        return READ_INVALID_BITS;

    struct image* img_pointer = image_create(header->biWidth, header->biHeight);
    *img = *img_pointer;
    free(img_pointer);

    uint8_t padding = calculate_padding(header->biWidth);
    uint32_t unpadded_row_length = img->width;
    uint32_t padded_row_size = sizeof(struct pixel)*unpadded_row_length + padding;

    for(uint32_t i=0; i<header->biHeight; i++) {
        fseek(in, (int32_t)(sizeof(struct bmp_header)+padded_row_size*i), SEEK_SET);
        if(!fread(img->data+i*unpadded_row_length, sizeof(struct pixel), img->width, in)){
            free(header);
            return READ_ERROR;
        }

    }
    free(header);
    return READ_OK;
}

enum read_status from_bmp_path(const char* fname, struct image* img) {
    FILE *file;
    if (open_file_read(fname, &file) != OPEN_OK) {
        return READ_INVALID_FILE;
    }
    enum read_status read_result = from_bmp(file, img);
    if (read_result != READ_OK) {
        return read_result;
    }
    if(fclose(file))
        return READ_CANT_CLOSE;
    return READ_OK;
}

struct bmp_header* create_bmp_header( struct image const* img ) {
    struct bmp_header* header = malloc(sizeof(struct bmp_header));
    struct bmp_header zero = {0};
    *header = zero;
    header->bfType = STANDART_BF_TYPE;
    header->bfileSize = sizeof(struct bmp_header) + (sizeof(struct pixel)*img->width + calculate_padding(img->width))*img->height;
    header->bOffBits = sizeof(struct bmp_header);
    header->biSize = HEADER_SIZE;
    header->biWidth = img->width;
    header->biHeight = img->height;
    header->biPlanes = STANDART_PLANES;
    header->biBitCount = sizeof(struct pixel)*BITS_IN_BYTE;
    header->biSizeImage = sizeof(struct pixel)*img->width*img->height;
    header->biXPelsPerMeter = STANDART_PELS_PER_PIXEL;
    header->biYPelsPerMeter = STANDART_PELS_PER_PIXEL;
    return header;
}

enum write_status to_bmp( FILE* out, struct image const* img ) {
    if(!out)
        return WRITE_INVALID_FILE;
    struct bmp_header* header = create_bmp_header(img);
    if(!fwrite(header, sizeof(struct bmp_header), 1, out)){
        free(header);
        return WRITE_ERROR;
    }
    uint8_t padding = calculate_padding(img->width);
    uint32_t const filler = 0;
    for(uint32_t i=0; i<img->height; i++) {
        if(!fwrite(img->data+i*img->width, sizeof(struct pixel)*img->width, 1, out)) {
            free(header);
            return WRITE_ERROR;
        }
        if(!fwrite(&filler, padding, 1, out)) {
            free(header);
            return WRITE_ERROR;
        }
    }
    free(header);
    return WRITE_OK;
}

enum write_status to_bmp_path(const char* fname, struct image const* img) {
    FILE *file;
    if (open_file_write(fname, &file) != OPEN_OK) {
        return WRITE_INVALID_FILE;
    }
    enum write_status write_result = to_bmp(file, img);
    if (write_result != WRITE_OK) {
        return write_result;
    }
    if(fclose(file))
        return WRITE_CANT_CLOSE;
    return WRITE_OK;
}
