#ifndef ASSIGNMENT_IMAGE_ROTATION_ROTATE_H
#define ASSIGNMENT_IMAGE_ROTATION_ROTATE_H
#include "image.h"

void image_rotate(struct image const *image, struct image *out_image);

#endif//ASSIGNMENT_IMAGE_ROTATION_ROTATE_H
