//
// Created by volt200211 on 26.01.2022.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_TRANSFORM_H
#define ASSIGNMENT_IMAGE_ROTATION_TRANSFORM_H

#include "image.h"

void transform(struct image const *src, struct image* dst, void (*f)(struct image const* image, struct image* out_image));

#endif //ASSIGNMENT_IMAGE_ROTATION_TRANSFORM_H
