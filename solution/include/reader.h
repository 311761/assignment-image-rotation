#ifndef ASSIGNMENT_IMAGE_ROTATION_READER_H
#define ASSIGNMENT_IMAGE_ROTATION_READER_H

#include "image.h"
#include <stdio.h>

/*  deserializer   */
enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_INVALID_FILE,
    READ_CANT_CLOSE,
    READ_ERROR
};

enum read_status from_bmp( FILE* in, struct image* img );

enum read_status from_bmp_path( const char* fname, struct image* img);

/*  serializer   */
enum write_status  {
    WRITE_OK = 0,
    WRITE_INVALID_FILE,
    WRITE_ERROR,
    WRITE_CANT_CLOSE
};



enum write_status to_bmp( FILE* out, struct image const* img );

enum write_status to_bmp_path( const char* fname, struct image const* img);


#endif //ASSIGNMENT_IMAGE_ROTATION_READER_H
