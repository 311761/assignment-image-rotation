#ifndef ASSIGNMENT_IMAGE_ROTATION_FILE_IO_H
#define ASSIGNMENT_IMAGE_ROTATION_FILE_IO_H

#include <stdio.h>

enum open_status {
    OPEN_OK = 0,
    OPEN_INVALID_PATH,
    OPEN_ERROR
};

enum open_status open_file_read(const char *fname, FILE **stream);
enum open_status open_file_write(const char *fname, FILE **stream);

#endif//ASSIGNMENT_IMAGE_ROTATION_FILE_IO_H
