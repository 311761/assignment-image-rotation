#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_H

#include <inttypes.h>
#include <stdbool.h>

#pragma pack(push, 1)
struct pixel { uint8_t b, g, r; };
#pragma pack(pop)


struct image {
    uint32_t width, height;
    struct pixel* data;
};

struct pixel* pixel_create(uint8_t r, uint8_t g, uint8_t b);
uint8_t* pixel_as_array(struct pixel *p);
struct image* image_create(uint64_t width, uint64_t height);
struct image* image_create_filled(uint64_t width, uint64_t height, struct pixel* color);

struct pixel* image_get_pixel(struct image const *im, uint32_t x, uint32_t y);
bool image_set_pixel(struct image *im, uint32_t x, uint32_t y, struct pixel value);

void image_free(struct image* im);

#endif //ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
